<?php
/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */

$GLOBALS['TL_LANG']['tl_content']['icon_legend'] = 'FontAwesome5 Icon-Einstellungen';

// Fields
$GLOBALS['TL_LANG']['tl_content']['faIcon'] = array('Icon Picker (FontAwesome v' . FONTAWESOME_VERSION . ')', 'W&auml;hlen Sie ein Icon aus der Font Awesome 5 Bibliothek aus.');
$GLOBALS['TL_LANG']['tl_content']['iconClass'] = array('Zus&auml;tzliche CSS-Klasse f&uuml;r das Icon', 'Z.B.: fa-4x');


// References
$GLOBALS['TL_LANG']['tl_content']['far'] = 'Regular';
$GLOBALS['TL_LANG']['tl_content']['fas'] = 'Solid';
$GLOBALS['TL_LANG']['tl_content']['fal'] = 'Light';
$GLOBALS['TL_LANG']['tl_content']['fab'] = 'Brands';
