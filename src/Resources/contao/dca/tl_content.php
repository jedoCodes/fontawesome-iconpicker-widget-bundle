<?php

/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */


/**
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['faIcon'] = array(
    'search' => true,
    'inputType' => 'fontawesomeiconpicker',
    'eval' => array('doNotShow' => true),
    'sql' => "blob NULL",
);

$GLOBALS['TL_DCA']['tl_content']['fields']['faIconClass'] = array(
    'search'    => true,
    'inputType' => 'text',
    'eval'      => array('maxlength' => 255),
    'sql'       => "varchar(255) NOT NULL default ''",
);


$GLOBALS['TL_DCA']['tl_content']['fields']['faIconSpinner'] = array(
    'exclude' => true,
    'inputType' => 'checkbox',
    'default' => 1,
    'eval' => ['tl_class' => 'w50'],
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['faIconBorder'] = array(
    'exclude' => true,
    'inputType' => 'checkbox',
    'default' => 1,
    'eval' => ['tl_class' => 'w50'],
    'sql' => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['faIconRotate'] = array(
    'search'    => true,
    'inputType' => 'select',
    'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
    'options' => array('fa-rotate-90', 'fa-rotate-180', 'fa-rotate-270', 'fa-flip-horizontal', 'fa-flip-vertical'),
    'sql'       => "varchar(255) NOT NULL default ''",
);


$GLOBALS['TL_DCA']['tl_content']['fields']['faIconSize'] = array(
    'search'    => true,
    'inputType' => 'select',
    'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
    'options' => array('fa-lg', 'fa-xs', 'fa-sm', 'fa-1x', 'fa-2x', 'fa-3x', 'fa-4x', 'fa-5x', 'fa-6x', 'fa-7x', 'fa-8x', 'fa-9x', 'fa-10x'),
    'sql'       => "varchar(255) NOT NULL default ''",
);

