<?php
/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

// Palettes
PaletteManipulator::create()
    ->addLegend('fontawesome_legend', 'files_legend', PaletteManipulator::POSITION_AFTER)
    ->addField('customFontawesomeSRC', 'fontawesome_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');


// Fields
$GLOBALS['TL_DCA']['tl_settings']['fields']['customFontawesomeSRC'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_settings']['customFontawesomeSRC'],
    'inputType' => 'text',
    'eval' => array('mandatory' => false, 'tl_class' => 'w50')
);